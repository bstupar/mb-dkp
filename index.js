const express = require('express');
const app = express();
const path = require('path');

const port = 3000;


app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/dkp.html'));
});

app.listen(port);
console.log('app up on port ' + port);